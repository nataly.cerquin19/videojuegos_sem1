using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaballeroController : MonoBehaviour
{
    public float velocityX = 5;
    public float jumpForce = 10;

    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;
    
    private const int ANIMATION_IDLE = 0;
    private const int ANIMATION_ATTACK= 1;
    private const int ANIMATION_WALK= 2;
    private const int ANIMATION_RUN = 3;
    private const int ANIMATION_JUMP= 4;
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        animator.SetInteger("Estado", ANIMATION_IDLE);
        
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocityX, rb.velocity.y); 
            sr.flipX = false;
            animator.SetInteger("Estado",ANIMATION_WALK);
            if (Input.GetKey(KeyCode.X))
            {
                rb.velocity = new Vector2(rb.velocity.x + velocityX, rb.velocity.y); 
                animator.SetInteger("Estado",ANIMATION_RUN);   
            }
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocityX, rb.velocity.y); 
            sr.flipX = true;
            animator.SetInteger("Estado",ANIMATION_WALK);
            if (Input.GetKey(KeyCode.X))
            {
                rb.velocity = new Vector2(rb.velocity.x - velocityX, rb.velocity.y); 
                animator.SetInteger("Estado",ANIMATION_RUN);   
            }
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            animator.SetInteger("Estado",ANIMATION_JUMP);
        }
        if (Input.GetKey(KeyCode.Z))
        {
            animator.SetInteger("Estado",ANIMATION_ATTACK);
        }


    }
}
